public class Toy{
	
	public String name;
	public String type;
	public double cost;

	public void showType(){
		System.out.println("This toy is a " + this.type + ".");
	}
}