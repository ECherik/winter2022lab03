import java.util.Scanner;
public class Shop{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		Toy[] newToy = new Toy[4];
		
		for(int i = 0; i < newToy.length; i++){
			newToy[i] = new Toy();
			
			System.out.println("What is the name of the new toy?");
			newToy[i].name = reader.nextLine();
			
			System.out.println("What is the type of the new toy?");
			newToy[i].type = reader.nextLine();
			
			System.out.println("What is the cost of the new toy?");
			newToy[i].cost = Double.parseDouble(reader.nextLine());
		}
		
		System.out.println(newToy[3].name);
		System.out.println(newToy[3].type);
		System.out.println(newToy[3].cost);
		
		newToy[3].showType();
		
	}
}