public class Application{
	public static void main(String[]args){
		Snake snake1 = new Snake();
		Snake snake2 = new Snake();
		Snake[] den = new Snake[3];
		
		System.out.println(snake1.name = "Snakey");
		System.out.println(snake1.color = "Black");
		System.out.println(snake1.cost = 10000);
		
		System.out.println(snake2.name = "Snack");
		System.out.println(snake2.color = "Yellow");
		System.out.println(snake2.cost = 5000);
		
		snake1.sayName();
		snake2.sayName();
		
		den[0] = snake1;
		den[1] = snake2;
		
		System.out.println(den[0].name);
		
		den[2] = new Snake();
		
		System.out.println(snake1.name = "Sneakie ");
		System.out.println(snake1.color = "Dark blue");
		System.out.println(snake1.cost = 20000);
		
	}
	
}